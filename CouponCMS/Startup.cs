﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CouponCMS.Startup))]
namespace CouponCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
