﻿using CouponCMS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CouponCMS.Controllers
{
    public class HomeController : Controller
    {
        private CouponCMSContext db = new CouponCMSContext();
        public ActionResult Index()
        {
            return View(db.Coupons.ToList());
        }

        public ActionResult List()
        {
            ViewBag.Message = "Your application description page.";

            return View(db.Coupons.ToList());
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}