﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CouponCMS.Models
{
    public class Coupon
    {
        public int Id { get; set; }
        public Guid UserId { get; set; }
        public string Tittle { get; set; }
        public string Image { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string Link { get; set; }
        public DateTime Expire { get; set; }
    }
}